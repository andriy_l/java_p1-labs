package com.mainacademy.oop;

/**
 * Created by andriy on 26.04.17.
 * @author Andriy L
 */
public interface Ainf {
    /**
     * method inside interface
     */
    default void m(){}
}
