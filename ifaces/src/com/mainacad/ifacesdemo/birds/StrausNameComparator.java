package com.mainacad.ifacesdemo.birds;

import java.util.Comparator;

public class StrausNameComparator implements Comparator {

    @Override
    public int compare(Object o1, Object o2) {
        Straus s1 =  (Straus) o1;
        Straus s2 = (Straus) o2;
        String straus1Name = s1.getName();
        String straus2Name = s2.getName();
        return straus1Name.compareTo(straus2Name);
    }
}
