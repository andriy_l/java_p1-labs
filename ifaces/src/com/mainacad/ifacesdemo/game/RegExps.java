package com.mainacad.ifacesdemo.game;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by andriy on 22.05.17.
 */
public class RegExps {
    public static void main(String[] args) {
        String str = "aaaabaaaabaaa1fdsfa2fdsfs4fdsfa5fdsfsdfa6";
        Pattern pattern = Pattern.compile("(\\p{all})");
        Matcher matcher = pattern.matcher(str);
        System.out.println(matcher.groupCount());
    }
}
