package com.mainacad.ifacesdemo.birds;

import com.mainacad.ifacesdemo.foods.*;

import java.util.Comparator;

/**
 * Created by andriy on 17.05.17.
 */
public class Straus extends Bird implements Comparable{



    private boolean canFly = false;

    public Straus(String name, double weigth, int age) {
        super(name, weigth, age);
    }

    @Override
    public void fly() {
        if(canFly){
            System.out.println("I fly!");
        } else {
            System.out.println("I connot fly");
        }
    }

    public void run(){
        System.out.println("I'm running");
    }

    @Override
    public void eat(Food food) {
        if(food instanceof FastFood){
            this.setWeigth(this.getWeigth() * 1.9);
        } else if(food instanceof VitaminizedFood){
            canFly = true;
            this.setWeigth(this.getWeigth() * 1.4);
        } else if(food instanceof Insect){
            this.setWeigth(this.getWeigth() * 1.3);
        } else {
            this.setWeigth(this.getWeigth() * 1.01);
        }

    }

    @Override
    public void eatFF(FastFood food) {
        if(this.getAge() < 2 && this.getWeigth() < 30){
            System.out.println("I'm die. Bye-bye");;
        }
    }

    @Override
    public void eatVitamin(VitaminizedFood v) {
        System.out.println("vitamin eating");
    }

    @Override
    public int compareTo(Object o) {
        Straus that = (Straus) o;
        return this.getName().compareTo(that.getName());
    }
}
