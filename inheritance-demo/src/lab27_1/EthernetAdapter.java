package lab27_1;

import java.util.ArrayList;

/**
 * Created by andriy on 13.05.17.
 */
public class EthernetAdapter extends Device {
    int speed;
    String mac;
    public char hidingCode = 'b';
    @Override
    public void print(){
        System.out.println(hidingCode);
    }

    ArrayList a;
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EthernetAdapter)) return false;

        EthernetAdapter that = (EthernetAdapter) o;

        if (getSpeed() != that.getSpeed()) return false;
        return getMac() != null ? getMac().equals(that.getMac()) : that.getMac() == null;
    }

    @Override
    public int hashCode() {
        int result = getSpeed();
        result = 31 * result + (getMac() != null ? getMac().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName()+"{" +
                "speed=" + speed +
                ", mac='" + mac + '\'' +
                '}';
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }
    public EthernetAdapter(){

    }

    public EthernetAdapter(int speed, String mac) {
//        super("ASUS", 20, "9302809328");
        String str = super.getManufacturer();
        this.speed = speed;
        this.mac = mac+super.getManufacturer()+this.getManufacturer();
    }
}
