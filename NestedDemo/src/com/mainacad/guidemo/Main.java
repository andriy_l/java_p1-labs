package com.mainacad.guidemo;

public class Main {

    public static void main(String[] args) {
	PhoneBook phoneBook = new PhoneBook();
	phoneBook.addPhoneNumber("Andriy", "423423424234");
	phoneBook.addPhoneNumber("Vasyl", "4323424234");
	phoneBook.addPhoneNumber("Serhiy", "55500000000");
	phoneBook.addPhoneNumber("Stanislav", "99999999");
	phoneBook.addPhoneNumber("Ivan", "8888888888");

	PhoneBook.PhoneNumber phoneNumber1 = new PhoneBook.PhoneNumber("Viktor","99999");
	PhoneBook.PhoneNumber phoneNumber2 = new PhoneBook.PhoneNumber("Valentyn","934242");


	PhoneBook.Page pageA = phoneBook.new Page("A");
	PhoneBook.Page pageB = phoneBook.new Page("B");

	phoneBook.printPhoneBook();


    }
}
