package com.mainacad.ifacesdemo.foods;

/**
 * Created by andriy on 17.05.17.
 */
@FunctionalInterface
public interface VitaminizedFood extends EatableFood,TastyFood {
    String eatVitamins(int count);
}
