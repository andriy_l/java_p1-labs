package lab27_1;

/**
 * Created by andriy on 13.05.17.
 */
public class Monitor extends Device implements Cloneable{
    int resolutionX;
    int resolutionY;

    @Override
    public String toString() {
        return "Monitor{" +
                "resolutionX=" + resolutionX +
                ", resolutionY=" + resolutionY +
                '}';
    }

    public int getResolutionX() {
        return resolutionX;
    }

    public void setResolutionX(int resolutionX) {
        this.resolutionX = resolutionX;
    }

    public int getResolutionY() {
        return resolutionY;
    }

    public void setResolutionY(int resolutionY) {
        this.resolutionY = resolutionY;
    }

    public Monitor(int resolutionX, int resolutionY) {

        this.resolutionX = resolutionX;
        this.resolutionY = resolutionY;
    }

    public Object clone() throws CloneNotSupportedException{
        return super.clone();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Monitor)) return false;

        Monitor monitor = (Monitor) o;

        if (getResolutionX() != monitor.getResolutionX()) return false;
        return getResolutionY() == monitor.getResolutionY();
    }

    @Override
    public int hashCode() {
        int result = getResolutionX();
        result = 31 * result + getResolutionY();
        return result;
    }
}
