package samples;

/**
 * Created by andriy on 4/6/17.
 */
public class CastingPrimitiveDemo {
    public static void main(String[] args) {
        //
        //      CASTING
        //

        int n = 123456789;
        float f = n; // value of f = 1.234567892Е8
        System.out.println("n= " + n + " , f= " + f);

        double x = 9.997;
        int nx = (int)x;
        System.out.println(" x= " + x + " , Casted nx= " + nx);

            // better to use
        double x2 = 9.997d;
        int nx2 = (int) Math.round(x2); // round(double) -> long
        System.out.println(" x= " + x + " , Rounded nx= " + nx);

         // int b0 = 0;
        // boolean b1 = (boolean) b0; impossible to prevent errors, not like in C++

        // if out of range converted value it will be reduced:
        byte b = (byte) 300;
        long c = 2222222222L;
        byte z = (byte)c;

        Object o = new Object();
        String s = new String("fljsdalfkjasdfjak");
//        s = (String)o;


        System.out.println("Reduced 300 to byte: " + b);
        System.out.println("Reduced 255 to byte: " + (byte)255);
        System.out.println("Reduced 128 to byte: " + (byte)128);
        System.out.println("Reduced 127 to byte: " + (byte)127); // fit in byte



    }
}
