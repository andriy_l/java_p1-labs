package com.mainacad.reactive;

import java.util.List;
import java.util.concurrent.Flow;
import java.util.concurrent.SubmissionPublisher;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {
        Stream.of(1,2,1).dropWhile(val -> val < 2).forEach(System.out::println);

        // Optional

        // Completable Feature
            // 4 reactive interfaces

        Flow.Subscription subscription;
        // two main
        Flow.Subscriber subscriber;
        Flow.Processor processor;
        // sample of completable feature
        // публікуючий об'єкт
        SubmissionPublisher<Integer> x = new SubmissionPublisher<>();
        // підписник
        SubscriberDemo  subscriberDemo = new SubscriberDemo();
        x.subscribe(subscriberDemo);

        List.of(1,2,3,4,5,5).forEach(x::submit);
        try {
            Thread.sleep(1_000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        String string1 = "test";
        String string2 = new String("test");
        System.out.println(string1 == string2);

    }
}
