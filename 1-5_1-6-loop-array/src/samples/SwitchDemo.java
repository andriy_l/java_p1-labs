package samples;

/**
 * Created by andriy on 13.04.17.
 */
public class SwitchDemo {
    public static void main(String[] args) {
        char hour = 21;
        switch (hour){ // string
            case 4: case 5: case 6: case 7: case 8:case 9: {
                System.out.println("Morning");
                break;
            }
            case 12:case 13:case 14:case 15: {
                System.out.println("Afternoon");
                break;
            }
            case 18:case 21: {
                System.out.println("Evening");
                break;
            }
            default: {
                System.out.println("Hour is out of range");
            }


        }
    }
}
