package com.mainacad.ifacesdemo.wrappers;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by andriy on 24.05.17.
 */
public class WrapperDemo {
    static Integer n = 1;
    static int m;
    public static void main(String[] args) {
        double d = Double.parseDouble("4.4");
        Number i1 = new Integer(31);
        Number i2 = new Integer("-13");
        Number i3 = -3;
        Integer i3b = 4;
        Double d2 = new Double(".0999");
        Number i4 = 10L;
        Number i5 = 10.5;
        Object i6 = 4;
        Object[] arr = {1,2,3};
        String str = "12";
        Pattern pattern = Pattern.compile("[0-9]{0,5}");
        Matcher matcher = pattern.matcher(str);
        if(matcher.matches()) {
            int i7 = Integer.parseInt(str);
        }else{

        }

        n++;
        n--;
        n += n;
        n = n + n;
//        m(m);
//        m(n);


//        System.out.println(i5.getClass().getName());
//        double a = i5.doubleValue();
//        int b = i5.intValue();
//        Number[] array = new Number[]{i1,i2,i3};
//        System.out.println(Arrays.toString(array));
//        Arrays.sort(array);
//        System.out.println(Arrays.toString(array));
    }
    public static void m(int number){
        System.out.println(number);
    }
}
