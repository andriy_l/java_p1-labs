package ovrrdemo;

/**
 * Created by andriy on 15.05.17.
 */
public interface Wisdom {

    String getExperience();

    abstract String getExperience2();

    static String getAdvice(){
        return "Analyze, Observe and Make conclusions!";
    }

    default void teach(){
        System.out.println("I'm teaching");
    }

}
