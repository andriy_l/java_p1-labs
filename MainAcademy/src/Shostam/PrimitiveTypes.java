package Shostam;


public class PrimitiveTypes {
    public static void main(String[] args) {
        int a = 15;
        short b = 4;
        boolean bool = true;
        double d = 5.4;
        float f = 14.2f;
        char ch = 'a';
        long h = 1500000L;
        byte by = 14;
        System.out.println("int " + a);
        System.out.println("short " + b);
        System.out.println("boolean " + bool);
        System.out.println("double " + d);
        System.out.println("float " + f);
        System.out.println("char " + ch);
        System.out.println("long " + h);
        System.out.println("byte " + by);
    }
}
