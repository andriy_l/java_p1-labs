package us.gov.whitehouse;

import java.io.IOException;

/**
 * Created by andriy on 08.05.17.
 */
public class Bird extends Animal{
    public void sing(){
        System.out.println("i'm a bird.....))))))");
    }

    public void sleep(){
        System.out.println("i sleep on a tree");
    }

    public void create() {
        System.out.println("birds are creative");
    }


    public Fish create(String nameOfBird) {
        System.out.println("birds are creative");
        return new Fish();
    }

    public void eat(double i) {
        System.out.println("I like seed and worms");
    }
    public int eat(int i) {
        System.out.println("I like seed and worms");
        return i;
    }
    // перевизначення методів
    // - override - передбачає, що в нащадку така сама сигнатура як в батьку
    // (signature = name and parameters of method)
    // - повертає той самий тип даних
    // - модифікатор доступу може мати більший доступ, або такий самий, але не менший
    // - метод може повертати підкласи класів (або ті самі класи), що й батьківський метод,
    // але не суперклас
    // - метод може "прокидати" throws виняткову ситуацію, таку як метод батька,
    // або нижчу по ієрархії виняткових ситуацій
    public int eat(){
        return 9999;
    }

}
