package samples;

import java.util.Arrays;

/**
 * Created by andriy on 13.04.17.
 */
public class ArrayOfArrays {
    public static void main(String[] args) {
        int[] array1 = {1,2,3};
        String[] names = new String[]{"Andriy", "Olexiy", "Pavlo", "Yurii"};
        for (int i = 0; i < names.length; i++) {
            if(i%2 == 0) {
                names[i] = "Good boy "+names[i];
            }
        }
//        System.out.println(Arrays.toString(names));
//        System.out.println(names[0]);
//        names[0] = "Roman";
//            System.out.println(names[0]);
//        System.out.println(names[names.length-1]);
        array1 = new int[4];
//        System.out.println(array1[0]);
//        System.out.println(array1[array1.length-1]);

        int[] array2 = {1,2,3};
        int[] array3 = {1,2,3};
        int[] array444 = new int[2];
        System.arraycopy(array2,1,array444,3,array2.length);
        System.out.println(Arrays.toString(array444));

        int[][] array4 = {array1, array2, array3, array1, {}, {1,2,3,4,5,6,6,7}};
//        for (int i = 0; i < array4.length; i++) {
//
//        }
        // {1,2,3}; // 0: 0, 1, 2
        // {1,2,3}; // 1: 0, 1, 2
        // {1,2,3}; // 2: 0, 1, 2

////        System.out.println(array4[1][1]);
//        for (int i = 0; i < array4.length; i++) {
//            for (int j = 0; j < array4[i].length; j++) {
//                System.out.print(array4[i][j]+" ");
//            }
//
////            System.out.println(Arrays.toString(array4[i]));
//            System.out.println();
//        }

//        int[] newArray = System.arraycopy();
//        System.out.println("New array: "+ Arrays.toString(newArray));
//
//
//
//        int[][][] array5 = {array4, array4};
//        // primitive / object
//        int[] array222 = new int[10];
//        Arrays.fill(array222, 999);
//        System.out.println(Arrays.toString(array222));
//        Arrays.fill(array222, 4, 8, 777);
//        System.out.println(Arrays.toString(array222));
//        System.out.println(Arrays.equals(array2, array3));
//        Arrays.sort(array222);
//        System.out.println(Arrays.toString(array222));


//
//        //
//        int[][] array21 = new int[2][3]; // маємо 2 масиви по 3 елементи
//        int[][] array22 = new int[][] {{1,2,3},
//                                       {4,5,6}}; // маємо 2 масиви по 3 елементи

    }
}
