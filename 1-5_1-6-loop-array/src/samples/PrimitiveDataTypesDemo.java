package samples;


// strictrfp - precise computations
public class PrimitiveDataTypesDemo {

    // constants: final, or static final -t be Class constant
    public final int SOME_CONSTANT = 1000_000;
    public static final int SOME_CONSTANT_2 = 2000_000;

// strictrfp - precise computations
    public static strictfp void main(String[] args) {
        String binaryInt = "01010101101"; // String literal
        int integerV0 = Integer.parseInt(binaryInt, 2);
        System.out.println(binaryInt + " is in decimal " + integerV0);
        int integerV1 = 1_000_000;
        System.out.println(integerV1 + " is in binary: " + Integer.toBinaryString(integerV1));
        int integerV2 = 0b0000_0001_0000; // int in bin representation
        System.out.println(integerV2 + " is in binary " + Integer.toBinaryString(integerV2));
        int integerV3 = 0x9FFFFFF; // int in hex representation
        System.out.println(integerV3 + " is in hex " + Integer.toHexString(integerV3));
        int integerV4 = 077777; // int in octal representation
        System.out.println(integerV4 + " is in octal " + Integer.toOctalString(integerV4));

        double doubleVal0 = .1;
        double doubleVal1 = .2;
        double doubleVal2 = doubleVal1 + doubleVal0;
        double doubleValZero = 0;

        System.out.println("Because of 64bit number representation: 0.1 + 0.2 " + doubleVal2);
        System.out.println("Because of 64bit number representation: 2.0 - 1.1 " + (2.0-1.1));
        System.out.println("This is error of rounding. For financial calculations use BigDecimal!");
        System.out.println(doubleVal0 + " hex representation " + Double.toHexString(doubleVal0));
        System.out.println("0.1/0.0 = " + doubleVal0 / doubleValZero);
        System.out.println("0.0/0.1 = " + doubleValZero / doubleVal0);
        double doubleValNaN = doubleValZero / doubleValZero;
        System.out.println("0.0/0.0 = " + doubleValNaN + ", if x is NaN, x == Double.Nan alwasy give" + (doubleVal0 == Double.NaN));
        System.out.println("use Double.isNaN(): " + Double.isNaN(doubleValNaN));
        int intVal3 = 3;
        int intVal4 = 2;
        double doubleVal5 = intVal3 / intVal4;
        System.out.println("Because of data size " + doubleVal5);

        char ch = '\uu03C0';

        // public static void main(String\u005B\u005D args) - possible to use in sources ( [ ])
        //        \b - back on 1 position
        //        \t - tab, \r caret
        System.out.println(ch + " isAlphabetic? " + Character.isAlphabetic(ch));

        // variable names: possible but not recommended to start with $





    }
}
