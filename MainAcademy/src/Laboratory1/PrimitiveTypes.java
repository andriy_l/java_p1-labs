package Laboratory1;


public class PrimitiveTypes {
    public static void main(String[] args) {
        int variable = 4;
        String variableAsAString = variable + ""; //concatenate
        System.out.println("bin: " + Integer.parseInt("1001010",2));
        System.out.println("oct: " + Integer.parseInt("0123",8));
        System.out.println("dec: " + Integer.parseInt("97987",10));
        System.out.println("hex: " + Integer.parseInt("FFFF",16));
    }
}
