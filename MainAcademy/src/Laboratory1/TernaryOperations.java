package Laboratory1;

public class TernaryOperations {
    public static void main(String[] args) {
        int a = 3;
        int b = 5;
        String result = a > b       ?       "gt" : "lt";
                        //condition ?        true : false
        System.out.println(result);
        int result2 = (a == b) ? 0 : (a < b) ? 1 : -1;
        System.out.println();
    }
}
