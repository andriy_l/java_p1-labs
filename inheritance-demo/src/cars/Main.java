package cars;

/**
 	Override sample
*/


class A{
    public char a = 'a';
    @Override
    public String toString(){
       return  "Hi im A";
    }
    public char getVal(){
        return a;
    }
}

class B extends A{
    public char a = 'b';
    @Override
    public String toString(){
        return  "Hi im B";
    }

}

class C extends B{
    public char a = 'c';
    public void printZ(){
        System.out.println("z");
    }

}
public class Main {
public static void main(String[] args) {
        A a = new A();
        System.out.println(a.toString() + " getVal: " + a.getVal() + " field a:" + a.a);
        // Hi im A getVal: a field a:a

        A ab = new B();
        System.out.println(ab.toString() + " getVal: " + ab.getVal() + " field a:" + ab.a); // Hi im B getVal: a field a:a

        B b = new B();
        System.out.println(b.toString() + " getVal: " + b.getVal() + " field a:" + b.a);
        // Hi im B getVal: a field a:b

        A ac = new C();
        System.out.println(ac.toString() + " getVal: " + ac.getVal() + " field a:" + ac.a);
        // Hi im B getVal: a field a:a
//    System.out.println("z: "+ac.printZ());

        C c = new C();
        System.out.println(c.toString() + " getVal: " + c.getVal() + " field a:" + c.a);
        // Hi im B getVal: a field a:c
    }
}
