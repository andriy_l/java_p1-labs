package samples;

/**
 * Created by andriy on 13.04.17.
 */
public class VariablesUsage {
    static final int B = 4;

    public static void main(String[] args) {
        final int A = 4;

        System.out.println("My PI is: " + Constants.MYPI2); // constant of class
        // Constants name of class, class - це зсилочний тип даних
        // c - instance (екземпляр класу), назва змінної
        // new - ключове слово, яке вказує на ініціалізацію змінних класу,
        // __________виділення пам'яті_______________
        // Contstants() - constructor - такий собі "метод", який ініціалізує змінні класу
        Constants c = new Constants();
        System.out.println("My PI2 is: " + c.MYPI); // constant of object
        // class Math
        System.out.println(Math.PI);
        // class StrictMath
    }
}
