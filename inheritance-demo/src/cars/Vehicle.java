package cars;

public class Vehicle {
    String name;
    long id;
    int maxSpeed = 200;

    public Vehicle(){
        this("MegaVehicle");
    }

    public Vehicle(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Vehicle{" +
                "name='" + name + '\'' +
                ", id=" + id +
                ", maxSpeed=" + maxSpeed +
                '}';
    }

    @Override
    public boolean equals(Object v){
        if(v instanceof Vehicle){
            if(this.name.equals(((Vehicle) v).name)) {
                return true;
            }else{
                return false;
            }
        } else {

        return false;
        }
    }

    public int hashCode(){
        return name.hashCode();
    }

}
