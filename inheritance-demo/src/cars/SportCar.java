package cars;

/**
 * Created by andriy on 10.05.17.
 */
public class SportCar extends Vehicle{
    String name;
    long id = 4L;
    int maxSpeed = 250;

    public SportCar(String name) {
        this.name = name;
    }

    public SportCar() {
        this("SportCarrrr");
    }
    public int getX(){
        return 250;
    }

    @Override
    public String toString() {
        return "SportCar{" +
                "name='" + name + '\'' +
                ", id=" + id +
                ", maxSpeed=" + maxSpeed +
                '}';
    }
}
