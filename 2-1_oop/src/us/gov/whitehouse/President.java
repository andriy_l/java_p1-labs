package us.gov.whitehouse;

/**
 * клас, який описує президента
 */
public class President {
    // getXXX all
    // setXXX all
    // isXXX  boolean
    private String name = "Tramp";
    private String secondName;
    private int a;
    private boolean merried;
    /**
     * пенсійний вік президента
     */
    final int PENSION_AGE = 100;
    /**
     * метод, який повертає ім'я
     * @return ім'я
     */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public boolean isMerried() {
        return merried;
    }

    public void setMerried(boolean merried) {
        this.merried = merried;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    private double amount = 50.300_000_000;

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    /**
     * метод обрахунку довжини імені
     * @see President#calculatLenghtOfName(String)
     * @param secondName
     * @return
     */
    public int calculatLenghtOfName(String secondName){

        return secondName.length()+name.length();
    }

    /**
     * Цей метод рахує довжини стрічок
     * @param secondName прізвище президента
     * @return кількість символів в імені і прізвищі преза
     */
    public int calculatLenghtOfName2(String secondName){

        return this.secondName.length()+name.length();
    }


}
