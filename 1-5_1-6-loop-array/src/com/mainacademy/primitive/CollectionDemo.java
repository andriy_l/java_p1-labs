package com.mainacademy.primitive;

import java.util.*;

// implementation of Comparable allows to sort
// we implement  THIS_STUDENT.compareTo(ANOTHER_STUDENT)
class Student implements Comparable{
    private String name;
    private long id;

    public Student(String name, long id) {
        this.name = name;
        this.id = id;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name=" + name +
                '}';
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public int compareTo(Object o) {
//        long a = 69302843204338L; // 8
//        int b = (int)a;  // 4
        Student student2 = (Student) o; // casting object to Student
        return this.getName().compareToIgnoreCase(student2.getName());
//        return this.getId() == student2.getId() ? 0 :
//                (this.getId() < student2.getId() ? -1 : 1);
    }
}

public class CollectionDemo {

    public static void main(String[] args) {
        Student std1 = new Student("Andriy", 100L);
        Student std2 = new Student("Charles", 300L);
        Student std3 = new Student("Augustine", 200L);
        Student std4 = new Student("Mohammed", 400L);
        Student std5 = new Student("Natalya", 500L);
        // array
        Student[] students = new Student[]{std1, std2, std3, std4};
        System.out.println(Arrays.toString(students));
        System.out.println("size of array: " + students.length);
        // Arrays - utility to work with arrays
        Arrays.sort(students); // very usufull class Arrays
        System.out.println(Arrays.toString(students));
        // list
        List<Student> listOfStudentsFixedSize = new ArrayList<>();
        // convert array to fixed-size list
        listOfStudentsFixedSize = Arrays.asList(students);
        List<Student> studentListNotFixedSize =
                new ArrayList<>(listOfStudentsFixedSize);
        studentListNotFixedSize.add(std5);
        // convert list to array
        Student[] students2 = (Student[])listOfStudentsFixedSize.toArray();
        studentListNotFixedSize.add(std5);
        studentListNotFixedSize.add(std3);
        studentListNotFixedSize.add(std1);
        studentListNotFixedSize.add(std2);
        System.out.println(studentListNotFixedSize);
        // sort collections in our case - list
        Collections.sort(studentListNotFixedSize);
        System.out.println(studentListNotFixedSize);
        // set collection for unique values
        Set<Student> uniqueStudentsUnsorted = new HashSet<>(studentListNotFixedSize);
        System.out.println(uniqueStudentsUnsorted);
        Set<Student> sortedUniqueStudents = new TreeSet<>(studentListNotFixedSize);
        System.out.println(sortedUniqueStudents);
        // Map <Key, Value>
        Map<String, Student> register = new LinkedHashMap<>();
        // Hash is organization of my keys, HashMap
        // Tree ordered key organization
        // LinkedHashMap - save in addition order
        register.put(std1.getName(), std1);
        register.put(std2.getName(), std2);
        register.put(std3.getName(), std3);
        System.out.println(register);
//        if(register.containsKey(std1.getName())){
//            Student value = register.get(std1.getName());
//            register.put(std1.getName(), ++value);
//
//        }
        register.put(std1.getName(), std4);
        register.put(std3.getName(), std5);
        System.out.println(register);
        // Mohammed - phone Map<String, String> - phone - full name
        // Charles - dictionary Map<String, String> - ukr_word - eng_word
        // Augustine - Words counter Map<Integer, String> - wordCount - word
        //






    }
}
