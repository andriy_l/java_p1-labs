package samples;

import java.util.Arrays;

/**
 * Created by andriy on 4/6/17.
 */
public class OperationsDemo {
    public static void main(String[] args) {
        // logical
        // shorthand
        int x = 0; // 10
        int y = -20;
        // якщо перший операнд визначає значення всього виразу, то інші операнди не обчислюються
        if (x != 0 && 1/x > x+y) { // якщо значення змінної x рівне нулю права частина не обчислюється
            System.out.println("Результат: " + x);
        }

        // priority
        int a = 2, b = 3, c = 4, d = 5, r;
        r = a + b * c - d;
        // 1) *
        // 2) + , -
        // 3) =

        // bit ops
        //
        // >> (Signed right shift)
        // All integers are signed in Java,
        // and it is fine to use >> for negative numbers.
        // The operator ‘>>’ uses the sign bit (left most bit) to fill the trailing positions after shift.
        // If the number is negative, then 1 is used as a filler and if the number is positive,
        // then 0 is used as a filler.
        // For example, if binary representation of number is 10….100,
        // then right shifting it by 2 using >> will make it 11…….1.
        //
        int xx = -4;
        System.out.println(xx + " in binary is "+Integer.toBinaryString(xx)
                +" after >>1 "+ (xx>>1)
                + " in bin " + Integer.toBinaryString(xx>>1));
        System.out.println();
        int yy = 4;
        System.out.println(yy + " in binary is "+Integer.toBinaryString(yy)
                +" after >>1 "+ (yy>>1)
                + " in bin " + Integer.toBinaryString(yy>>1));


        System.out.println();
        // << (Signed right shift)
        // x << n = x * 2^n

        int aa = 4;
        int nn = 10;
        int result = aa << nn;
        System.out.println(aa + " in binary is "+Integer.toBinaryString(aa)
                +" after << 10 "+ (result)
                + " in bin " + Integer.toBinaryString(result));
        System.out.println();
        // >>> (Unsigned right shift) In Java,
        // It always fills 0 irrespective of the sign of the number.
        // x is stored using 32 bit 2's complement form.
        // Binary representation of -1 is all 1s (111..1)
        int xxx = -1;
        int yyy1 = 29;
        int zzz1 = xxx>>>yyy1;
        int yyy2 = 30;
        int zzz2 = xxx>>>yyy2;
        int yyy3 = 31;
        int zzz3 = xxx>>>yyy3;


        System.out.println(xxx + " in binary is "+Integer.toBinaryString(xxx)
                +" after xxx >>> 29 "+ zzz1
                + " in bin " + Integer.toBinaryString(zzz1));
        // The value of 'x>>>29' is 00...0111  ===  7

        System.out.println(xxx + " in binary is "+Integer.toBinaryString(xxx)
                +" after xxx >>> 30 "+ zzz2
                + " in bin " + Integer.toBinaryString(zzz2));
        // The value of 'x>>>30' is 00...0011   === 3

        System.out.println(xxx + " in binary is "+Integer.toBinaryString(xxx)
                +" after xxx >>> 31 "+ zzz3
                + " in bin " + Integer.toBinaryString(zzz3));
        // The value of 'x>>>31' is 00...0001  === 1

    }
}
