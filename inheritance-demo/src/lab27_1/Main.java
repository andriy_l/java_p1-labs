package lab27_1;

/**
 * Created by andriy on 13.05.17.
 */
public class Main {
    public static void main(String[] args) throws CloneNotSupportedException {
        Device device2 = new EthernetAdapter(1000, "70:f1:a1:cd:0e:de");
        Device device3 = new EthernetAdapter(1000, "70:f1:a1:cd:0e:db");
        Device device4 = new EthernetAdapter(1000, "70:f1:a1:cd:0e:db");
        Object device6 = new EthernetAdapter(1000, "70:f1:a1:cd:0e:db");
        EthernetAdapter device5 = new EthernetAdapter(1000, "70:f1:a1:cd:0e:db");
            device2.print();
            device5.print();
        System.out.println(device5);

        Monitor monitor1 = new Monitor(600,800);

        Device monitor2 = (Device) monitor1.clone();
        Object monitor3 = monitor1.clone();

        System.out.println(monitor2);
        System.out.println(monitor3);
        System.out.println(monitor2.hashCode()==monitor3.hashCode());
    }
}
