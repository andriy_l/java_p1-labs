package us.gov.whitehouse;

/**
 * Created by andriy on 08.05.17.
 */
public class Nature {
    public static void main(String[] args) {
        Fish fishy = new Fish();
        Bird birdy = new Bird();

        Animal animal = new Animal();
        // is-a: Риба є твариною
        //Fish extends Animal
        Animal fishy2 = new Fish();
        Animal birdy2 = new Bird();
//        Fish fish3 = new Animal();
        // Fish extends Animal extends God
        God fish4 = new Fish();
        Animal animal1 = (Animal) fish4;
    // exception ClassCastException
        Fish fish7 = (Fish) animal; // привести до нижчого класу вищий по ієрархії
      //  приведення типів
        Bird bird5 = (Bird) fishy2;

        System.out.println(fish4 instanceof God);
        System.out.println(fish4 instanceof Animal);
        System.out.println(fish4 instanceof Bird);
        System.out.println(fish4 instanceof Fish);



        System.out.println("hello");


    }
}
