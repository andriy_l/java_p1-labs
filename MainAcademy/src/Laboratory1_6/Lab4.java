package Laboratory1_6;


import java.util.Arrays;

public class Lab4 {
    public static void main(String[] args) {
        int[] m = new int[]{10, 21, 5, 22, 9, 29, 25, 22, 11, 14, 8, 14};
        Arrays.sort(m);
        System.out.println(Arrays.toString(m));
        System.out.println(Arrays.binarySearch(m, 9));
    }
}
