package com.mainacad.ifacesdemo.foods;

@FunctionalInterface
public interface FastFood extends PoisonableFood,LargeCaloriesFood,TastyFood {
    void iLikeFastFoodMethod();
}
