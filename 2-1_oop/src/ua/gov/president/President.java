package ua.gov.president;

/**
 * Created by andriy on 26.04.17.
 */
public class President {
    private String name = "Poroshenko";
    private double amount = 90.500_000_000_000;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    // modif
    public double getAmount() {
        return amount;
    }

    //                              параметри методу - ЦЕ ТАМ ДЕ МИ ОПИСУЄМО МЕТОД

    public double getPresidentsTax(double dopomoga, double dopomoga2){
        double taxRate = 0.155;
        double eSv = 0.10;
        double tax = (amount * taxRate * eSv) - dopomoga - dopomoga2;
        return tax;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }


}
