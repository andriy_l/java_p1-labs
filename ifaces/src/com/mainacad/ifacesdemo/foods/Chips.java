package com.mainacad.ifacesdemo.foods;

import java.io.Serializable;

/**
 * Created by andriy on 17.05.17.
 */
public class Chips implements FastFood,Serializable,Cloneable {
    private String manufacturer;

    public Chips(){
        this("McDonalds");
    }

    public Chips(String manufacturer) {
        this.setManufacturer(manufacturer);
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    @Override
    public void iLikeFastFoodMethod() {
        System.out.println("iLikeFastFoodMethod");
    }
}
