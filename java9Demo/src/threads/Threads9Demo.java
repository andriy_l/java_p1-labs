package threads;

/**
 * Created by andriy on 05.06.17.
 */
public class Threads9Demo {
    {
        Thread.onSpinWait(); // замість wait() -  оптимальніше
    }

    // deprecated дуже сильно
    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }
}
