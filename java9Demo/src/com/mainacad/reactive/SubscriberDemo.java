package com.mainacad.reactive;

import java.util.concurrent.Flow;

/**
 * Created by andriy on 05.06.17.
 */
public class SubscriberDemo implements Flow.Subscriber<Integer> {
    private Flow.Subscription subscription;
    @Override
    public void onSubscribe(Flow.Subscription subscription) {
        System.out.println("subscribed");
        this.subscription = subscription;
        this.subscription.request(1);
    }

    @Override
    public void onNext(Integer item) {
        if(item == 3) throw  new RuntimeException();
        System.out.println(item);
        this.subscription.request(1);
    }

    @Override
    public void onError(Throwable throwable) {
        System.out.println(throwable);
    }

    @Override
    public void onComplete() {
        System.out.println("completed");
    }
}
