package com.mainacad.guidemo;

interface Doable{
        boolean doIt();
}

class Job{
    public boolean done(Doable variable){
        return variable.doIt();
    }
}

public class AnonymousDemo {
    public static void main(String[] args) {
        Job homework = new Job();
        Doable lab2_12_2 = new Doable() {
            @Override
            public boolean doIt() {

                return true;
            }
        };
        boolean labFinished = homework.done(lab2_12_2);

        Job homework2 = new Job();
        boolean lab2finished = homework2.done(new Doable() {
            @Override
            public boolean doIt() {
                return true;
            }
        });

        Job homework3 = new Job();
        boolean lab3finished = homework.done(() -> true);
    }
}
