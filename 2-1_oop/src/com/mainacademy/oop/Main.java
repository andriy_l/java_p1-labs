package com.mainacademy.oop;

import ua.gov.president.President;

/**
 * головний клас програми
 */
public class Main {

    /**
     * стартовий метод програми
     * @throws ArithmeticException можуть виникати такі проблемні ситуації
     * @param args мали би бути арг.ком рядка
     */
    public static void main(String[] args) {
        President poroh = new President();

        President yanyk = new President();
        President kuchma = new President();
        System.out.println(poroh == yanyk);
        System.out.println(poroh == kuchma);
        System.out.println(poroh == new President());
        double value1 = 300;
        double value2 = 300;
        //  аргументи методу - ЦЕ ТАМ ДЕ МИ ВИКОРИСТОВУЄМО МЕТОД
        double value3 = poroh.getPresidentsTax(value1, value2);
        System.out.println(value3);


    }
}
