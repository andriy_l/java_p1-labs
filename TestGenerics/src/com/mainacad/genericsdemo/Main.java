package com.mainacad.genericsdemo;

import javax.swing.*;

import static com.mainacad.genericsdemo.MyTestMethod.calcNum;

public class Main {

    public static void main(String[] args) {

//MyTuple myTuple0 = new MyTuple<String,Integer,Long>("str", 2, 10L);
//MyTuple<String,Integer,Long> myTuple1 = new MyTuple<>("str", 2, 10L);
//myTuple1.setA(10);
//MyTuple<Double,String,String> myTuple2 = new MyTuple<Double,String,String>(2.2,"rst","abc");
        Integer[] integers = {1,2,3,4,5};
        Double[] doubles = new Double[]{1.0,2.1,3.2,4.4,5.0};
        String[] strings = {"1","2","3"};
        Number[] numbers = {1,2.3,4,33.4,9L,19};
        int a = MyTestMethod.calcNum(integers,2);
        System.out.println(a);
//        int b = MyTestMethod.calcNum(strings,2);
//        System.out.println(b);
        int c = MyTestMethod.<Double>calcNum(doubles, 2.0);
        System.out.println(c);
        int d = MyTestMethod.<Number>calcNum(numbers, 2.0);
        System.out.println(d);
    }
}
