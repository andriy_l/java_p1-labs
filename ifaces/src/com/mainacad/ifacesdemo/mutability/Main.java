package com.mainacad.ifacesdemo.mutability;

public class Main {
    public static void main(String[] args) {
        ImmutableClass imm1 = new ImmutableClass(10,"a");
        MutableClass mm1 = new MutableClass(10,"a");
        ImmutableClass imm2 = imm1;
        MutableClass mm2 = mm1;
        mm2.setId(20);
        System.out.println(mm1 == mm2);
        System.out.println(imm1 == imm2);
        System.out.println(mm1.getId());
    }
}
