package Laboratory1_5;


import java.util.Scanner;

public class Lab7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter your number: ");
        int n = scanner.nextInt();
        System.out.println("Your perfect numbers are: ");
        for (int i = 1; i <= n; i++) {
            int sum = 0;
            for (int j = 1; j < i; j++) {
                if (i % j == 0){
                    sum+= j;
                }
            }
            if (sum == i){
                System.out.print(i + " ");
            }

        }
    }
}
