package us.gov.whitehouse;

import java.util.Arrays;

class A{
    A a = new A();
}
/**
 * Клас, що описує навчальні курси
 */
class Course{
    private String courseName;
    @Override
    public String toString() {
        return "Course{" +
                "courseName='" + courseName + '\'' +
                '}';
    }

    /**
     * Метод для отримання імені курсу
     * @return ім'я курсу
     */
    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }
}

class GrandPa{
    public GrandPa(){
        System.out.println("i'm granny dad");
    }
}

class Tatko extends GrandPa{
    public Tatko(){
        System.out.println("I'm tatko");
    }
}

/**
 * Клас, який описує студента
 *
 */
public class Student extends Tatko{
    private Student std;
    private String name;
    private long id;
    private double[] excellence;
    private double avgExcellence;
    private Course[] courses = new Course[3];
    private int courseEnrolledNum = 0;
    private Student[] friends = new Student[2];



    private Student(){
        this("student");

    }


    /**
     *
     * @param name
     */
    public Student(String name){
        this.name = name;
        System.out.println("Student with name parameter");
        // !!! виклик в конструкторі публічного методу
//        setName(name);
    }

    public Student(String name, long id){
        this.name = name;
        this.id = id;
    }

    // method-helper
    private double calculateAvgExcellence(){
        for (int i = 0; i < excellence.length; i++) {
            avgExcellence += excellence[i];
        }
        return avgExcellence/excellence.length;
    }

    /**
     * some stupid method
     */
    public void Student(){
        System.out.println("This stupid method");
    }

    public String toString(){
        return "This intstance is " + this.name;
    }

    public double getAvgExcellence(){
        return calculateAvgExcellence();
    }

    /**
     * Записатися на курс
     * @param course приймає об'єкт з курсом навчання
     * @return true якщо може записатися, false якщо ні
     */
    public boolean enrollToCourse(Course course){
        if(courseEnrolledNum > courses.length - 1){
            System.out.println("Num of courses are more than " + courses.length);
            return false;
        } else {
            courses[courseEnrolledNum++] = course;
            System.out.println("We successfully enrolled to " + course);
            return true;
        }
    }

    /**
     * виводить курси на які підписаний студент
     */
    public void printCourses(){
        System.out.println(Arrays.toString(courses));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double[] getExcellence() {
        return excellence;
    }

    public void setExcellence(double[] excellence) {
        this.excellence = excellence;
    }
}
