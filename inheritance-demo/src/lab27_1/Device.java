package lab27_1;

public class Device {
 private String manufacturer;
 private float price;
 private String serialNumber;

 public String hidingCode = "a";

    @Override
    public String toString() {
        return "Device{" +
                "manufacturer='" + getManufacturer() + '\'' +
                ", price=" + getPrice() +
                ", serialNumber='" + getSerialNumber() + '\'' +
                '}';
    }

    {
        Math.cos(1);
    }
    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public Device(String manufacturer, float price, String serialNumber) {
        this();
        this.setManufacturer(manufacturer);
        this.setPrice(price);
        this.setSerialNumber(serialNumber);
    }

    public void print(){
        System.out.println(hidingCode);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Device)) return false;

        Device device = (Device) o;

        if (Float.compare(device.getPrice(), getPrice()) != 0) return false;
        if (getManufacturer() != null ? !getManufacturer().equals(device.getManufacturer()) : device.getManufacturer() != null)
            return false;
        return getSerialNumber() != null ? getSerialNumber().equals(device.getSerialNumber()) : device.getSerialNumber() == null;
    }

    @Override
    public int hashCode() {
        int result = getManufacturer() != null ? getManufacturer().hashCode() : 0;
        result = 31 * result + (getPrice() != +0.0f ? Float.floatToIntBits(getPrice()) : 0);
        result = 31 * result + (getSerialNumber() != null ? getSerialNumber().hashCode() : 0);
        return result;
    }

    public Device(){}
}
