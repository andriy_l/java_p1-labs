package com.mainacad.ifacesdemo.birds;


import com.mainacad.ifacesdemo.foods.EatableFood;
import com.mainacad.ifacesdemo.foods.FastFood;
import com.mainacad.ifacesdemo.foods.Food;
import com.mainacad.ifacesdemo.foods.VitaminizedFood;

public abstract class Bird implements Comparable {
    public abstract void fly();
    public abstract void eat(Food food);
    public abstract void eatFF(FastFood food);
    public abstract void eatVitamin(VitaminizedFood v);
    private String name;
    private double weigth;
    private int age;

    public Bird(String name, double weigth, int age) {
        this.name = name;
        this.weigth = weigth;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeigth() {
        return weigth;
    }

    public void setWeigth(double weigth) {
        this.weigth = weigth;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Bird{" +
                "name='" + name + '\'' +
                ", weigth=" + weigth +
                ", age=" + age +
                '}';
    }

    public int makeEggs(){
        if(age > 2 && weigth > 30)
            return 3;
        else
            return 2;
    }
}
