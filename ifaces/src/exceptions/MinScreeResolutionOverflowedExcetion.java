package exceptions;

/**
 * Created by andriy on 29.05.17.
 */
public class MinScreeResolutionOverflowedExcetion extends UIException {
    public MinScreeResolutionOverflowedExcetion() {
    }

    public MinScreeResolutionOverflowedExcetion(String message) {
        super(message);
    }

    public MinScreeResolutionOverflowedExcetion(String message, Throwable cause) {
        super(message, cause);
    }

    public MinScreeResolutionOverflowedExcetion(Throwable cause) {
        super(cause);
    }

    public MinScreeResolutionOverflowedExcetion(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
