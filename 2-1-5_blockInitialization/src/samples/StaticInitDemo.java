package samples;

/**
 * Created by andriy on 5/4/17.
 */
public class StaticInitDemo {
    static int i;
    {
        this.i = 5;
    }

    void m(){
        this.i++;
    }
}
