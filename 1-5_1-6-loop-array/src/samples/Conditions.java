package samples;

/**
 * Created by andriy on 13.04.17.
 */
public class Conditions {
    public static void main(String[] args) {
        int a = 3;
        int b = 6;
        int c = 8;
        // !!!(true||...)
        // !!!(false&&...)
        if(c < b) {
            System.out.println(" c lt b");

        } else if(++a%2 == 0){
            System.out.println("c not less than b");
            System.out.println(", a=" + a + ", a is even");
        }
        System.out.println(a);

    }
}
