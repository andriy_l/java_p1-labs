package ovrrdemo;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;

class Sex{
    String stat = "male";
}
public class Child extends Parent implements Mindset,Wisdom,Cloneable,Serializable{
    private int age;
    private Sex hender;

    public Child(){}
    public Child(int age, Sex hender) {
        this.age = age;
        this.hender = hender;
    }

    public void assemblerCoding() throws FileNotFoundException,ClassNotFoundException {
        System.out.println("I know C");
        return;
    }

    @Override
    public double swim() {
        return 0;
    }

    public Object clone() {
        // deep clone
        int ageForClone = this.age;
        Sex hender2 = new Sex();
        String stat2 = new String(this.hender.stat);
        hender2.stat = stat2;
        // deep clone2
        // запис об'єкту в потік (серіалізація) і читання з потоку (десеріалізація)
        return new Child(ageForClone, hender2);
        // shallow clone
        //return new Child(this.age, this.hender);
    }

    @Override
    public String getExperience() {
        return "Im so wise....";
    }

    @Override
    public String getExperience2() {
        return getExperience();
    }
}
