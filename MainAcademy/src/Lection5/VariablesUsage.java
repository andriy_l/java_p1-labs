package Lection5;


import sun.security.pkcs11.wrapper.Constants;

public class VariablesUsage {
    static final int B = 4;
    public static void main(String[] args) {
        System.out.println("My PI is: " ); // constant of class
        // Constants name of Class - це зсилочний тип даних
        // c - Instance (екземпляр класу), назва зміної
        // new - ключове слово, яке вказує на ініціалізацію змінних класу
        // ________виділення пам'яті________
        // Constants() - constructor - такий собі "метод", який ініціалізує змінні клау

        Constants c = new Constants();
        System.out.println("My PI is: " ) ; // constant of object
        // class Math
        System.out.println(Math.PI);
        // class StrictMath
    }
}
