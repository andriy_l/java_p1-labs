package com.mainacad.genericsdemo;

import java.util.Objects;

class DemoGeneric<Andriy>{
    private Andriy name;

    @Override
    public boolean equals(Object o) {
        if (this == o){
            System.out.println("Обєкт той самий");
            return true;};
        if (!(o instanceof DemoGeneric)) {
            System.out.println("not instance of "+DemoGeneric.class.getSimpleName() );
            return false;
        }
        DemoGeneric<?> that = (DemoGeneric<?>) o;
        System.out.println("comparison...");
        return Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "DemoGeneric{" +
                "name=" + name +
                '}';
    }

    public DemoGeneric(Andriy name) {
        this.name = name;
    }

    public DemoGeneric(){

    }
}

public class MyTuple<A,B,C> {
    private A a;
    private B b;
    private B c;

    public MyTuple(A a, B b, B c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public void setA(B a){
        System.out.println(a);
    }

    public A getA() {
        return a;
    }

    public B getB() {
        return b;
    }

    public B getC() {
        return c;
    }
}
