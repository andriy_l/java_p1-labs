package com.mainacad.ifacesdemo.foods;

/**
 * Created by andriy on 17.05.17.
 */
public class Corn implements LargeCaloriesFood,TastyFood {
    private String typeOfCorn;

    public Corn(){
            this("wheat");
    }

    public Corn(String typeOfCorn) {
        this.typeOfCorn = typeOfCorn;
    }

    public String getTypeOfCorn() {
        return typeOfCorn;
    }

    public void setTypeOfCorn(String typeOfCorn) {
        this.typeOfCorn = typeOfCorn;
    }
}
