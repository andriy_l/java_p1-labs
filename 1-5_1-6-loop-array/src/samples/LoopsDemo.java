package samples;

import java.util.Arrays;

/**
 * Created by andriy on 13.04.17.
 */
public class LoopsDemo {
    static int [] bbb;

    public static void main(String[] args) {
        int arraySize = 3;
        int[] array1d = new int[arraySize];

        System.out.println(array1d.length);
        array1d[0] = 2;
        array1d[1] = 4;
        array1d[2] = 6;
        System.out.println(array1d[0]);
        System.out.println(array1d[1]);
        System.out.println(array1d[2]);
        System.out.println(array1d[array1d.length-1]);
        array1d = new int[]{9, 99, 999, 9999, 99999};
//        array1d = {9, 99, 999, 9999, 99999}; // because of size 3
        int[] array1d2 = {9, 99, 999, 9999, 99999};
        System.out.print("[");
        for (int i = 0; i < array1d2.length ; i++) {
            System.out.print(array1d2[i] + (i==array1d2.length-1?"":", "));
        }
        System.out.println("]");
        System.out.print("[");
        for (int i = array1d2.length-1; i >= 0 ; i--) {
            System.out.print(array1d2[i] + (i==0?"":", "));
        }
        System.out.println("]");

        // read-only array access
        int y = 0;
        for (int accumulator: array1d2) {
            System.out.println(accumulator);
            y += accumulator;
        }
        System.out.println(y);

        for (int i = 0; i < array1d2.length; i++) {
            array1d2[i] = array1d2[i]*2;
        }
        System.out.println(Arrays.toString(array1d2));



    }
}
