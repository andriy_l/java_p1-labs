package moverloading;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.stream.IntStream;
import java.util.stream.Stream;

class A {
    IntStream intStream = IntStream.iterate(10, (x) -> {return x + 1;})
                                   .peek(System.out::println)
                                   .unordered()
                                   .map((x) -> {return x+x;}).limit(10);

    public A(){
        intStream.mapToObj((x -> Integer.toBinaryString(x));
    }

}


public class MOverloadingDemo {
    public static void main(String[] args) {

    }


}
