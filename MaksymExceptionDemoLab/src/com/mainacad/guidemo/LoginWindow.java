package com.mainacad.guidemo;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by andriy on 29.05.17.
 */
public class LoginWindow {
    private JTextField loginField;
    private JPasswordField passdField;
    private JButton loginButton;
    private JButton cancelButton;
    private JPanel basePanel;

    public static void main(String[] args) {
        JFrame frame = new JFrame("LoginWindow");
//        System.out.println("test");
        frame.setContentPane(new LoginWindow().basePanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public LoginWindow() {
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        loginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String msg = loginField.getText();
                String pass = new String( passdField.getPassword());
                JOptionPane.showMessageDialog(null,"Hello "+msg+" with pass "+pass,"Welcome to system msg", JOptionPane.INFORMATION_MESSAGE);
            }
        });
    }
}
