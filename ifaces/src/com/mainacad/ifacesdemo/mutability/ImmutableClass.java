package com.mainacad.ifacesdemo.mutability;

/**
 * Created by andriy on 27.05.17.
 */
public class ImmutableClass {
    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public ImmutableClass(int id, String name) {

        this.id = id;
        this.name = name;
    }
}
