package Laboratory1_5;


import java.util.Scanner;

public class Lab6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter your number: ");
        int a = scanner.nextInt();
        int sum = 0;
        do {
            sum += (a % 10) * (a % 10);
            a = a / 10;

        }
        while (a != 0);
        System.out.println("Sum of square digits: " + sum);
    }
}
