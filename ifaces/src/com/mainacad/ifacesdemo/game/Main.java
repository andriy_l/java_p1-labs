package com.mainacad.ifacesdemo.game;

import com.mainacad.ifacesdemo.birds.Bird;
import com.mainacad.ifacesdemo.birds.Duck;
import com.mainacad.ifacesdemo.birds.Straus;
import com.mainacad.ifacesdemo.birds.StrausNameComparator;
import com.mainacad.ifacesdemo.foods.*;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        Bird vasylko = new Straus("Vasylyna",10,2);
        vasylko.setName("Vasylyna");
        vasylko.setWeigth(10);
        vasylko.setAge(2);

        Bird hrystynka = new Duck("Hrystynka", 4, 2);
        Bird irynka = new Duck("Irynka", 3, 1);
        Bird maryanka = new Duck("Maryanka", 5, 3);

        Straus[] strauses = new Straus[]{
                new Straus(new String("Yovan"), 20, 2),
                new Straus(new String("Yovan"), 32, 2),
                new Straus("Petro",30,20),
                new Straus("Petro",10, 1)};
//        Arrays.sort(strauses,new StrausNameComparator());
//        System.out.println(Arrays.toString(strauses));
//        Bird[] kachechky = new Bird[]{hrystynka,irynka,maryanka};
//        System.out.println(Arrays.toString(kachechky));
//        Arrays.sort(kachechky);
//
//        System.out.println(Arrays.toString(kachechky));

        System.out.println(strauses[0].equals(strauses[1]));
        System.out.println(strauses[0] == strauses[1]);
        System.out.println((strauses[0].getName()).equals(strauses[1].getName()));
        System.out.println((strauses[0].getName()) == (strauses[1].getName()));
        System.out.println((strauses[3].getName()) == (strauses[2].getName()));
//        System.out.println((strauses[0].getName()).equals(strauses[1].getName()));


// екземпляри можна використовувати в програмі багато разів
//        Corn seed = new Corn();
//        Food ivanko = new Worm();
//        FastFood chipses = new Chips();
//        Food coka = new Cola();
//
//        vasylko.eat(seed);
//        vasylko.eat(ivanko);
//        vasylko.eat(chipses);
//        vasylko.eat(coka);
//// якщо потрібно тільки один єдиний екзепляр і тільки один раз,
////        то можна створити анонімний екземпляр класу
////        ....то можна створити анонімний екземпляр інтерфейсу
//
//
//        FastFood burger = new FastFood() {
//            @Override
//            public void iLikeFastFoodMethod() {
//                System.out.println("iLikeFastFoodMethod");
//            }
//            // реалізація інтерфейсу
//        };
//
//        vasylko.eat(new Worm());
//        vasylko.eat(new FastFood() {
//            @Override
//            public void iLikeFastFoodMethod() {
//                    System.out.println("iLikeFastFoodMethod");
//            }
//
//        });
//
//        vasylko.eatFF(chipses);
//        vasylko.eatFF(new FastFood() {
//                          @Override
//                          public void iLikeFastFoodMethod() {
//
//                          }
//                      }
//
//        );
//// функціональний інтерфейс,
////        інтерфейс тільки з одним методом
//        vasylko.eatFF(() -> {
//            System.out.println("Crisps..... eating");
//                      }
//        );
//
//        vasylko.eatVitamin(x -> "I'm healthy on " + x + "times");
//
//        vasylko.eatVitamin(new VitaminizedFood() {
//            @Override
//            public String eatVitamins(int x) {
//                return "I'm healthy on " + x + "times";
//            }
//        });
//
//
//
//
//
//        System.out.println("Our "
//                + vasylko.getClass().getSimpleName()
//                + " with name "
//                + vasylko.getName()
//                + " create eggs: "
//                + vasylko.makeEggs());
//

    }
}
