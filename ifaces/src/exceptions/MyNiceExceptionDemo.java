package exceptions;

import java.io.*;
import java.net.MalformedURLException;
import java.nio.file.AccessDeniedException;
import java.nio.file.FileSystemException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.rmi.AccessException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
class MyNiceExceptionDemoSon extends MyNiceExceptionDemo{

    public int getVal() throws IOException{
        return 0;
    }
}
public class MyNiceExceptionDemo {
    final static Logger LOGGER = Logger.getLogger(MyNiceExceptionDemo.class.getName());

    public static void main(String[] args){
        // unchecked exception

        int a = 4;
        int b = 0;
        int c = 0;
        int[] arr = new int[1];
        int z = 0;
        try {
//            c = a / b;
//            z = arr[2];
//            byte[] fileContents = Files.readAllBytes(Paths.get("file.jpg"));
            String text = new String(Files.readAllBytes(Paths.get("file.txt")));
            throw new OutOfMemoryError();
        }catch(OutOfMemoryError error){
            LOGGER.log(Level.WARNING,error.toString());
            error.printStackTrace();
        }catch (ArithmeticException|ArrayIndexOutOfBoundsException|AccessDeniedException ade) {
            ade.printStackTrace();
        }catch(FileSystemException|NullPointerException fs){
            LOGGER.log(Level.WARNING,fs.toString());
            fs.printStackTrace();
        }catch (IOException m){
            m.printStackTrace();
        }catch (Exception e){
            System.out.println(e);
        }catch (Throwable t){
            System.out.println(t);
        }finally{
            System.out.println("Ділити на нуль не можна!!!!(цілі числа)");
        }
        System.out.println("Have a nice day!");
        LOGGER.log(Level.INFO,"Have a nice day!");

        // checked exception
        File file = new File("file.txt");
        Scanner scanner = null;
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(file));
            int aaaa = br.read();
        }catch(FileNotFoundException fnfe) {
            System.out.println(fnfe);
//            fnfe.printStackTrace();
        }catch (IOException ioe){
            System.out.println("сталося" + ioe);
        }finally {
            try {
                br.close();
            }catch (IOException ioe){
                System.out.println(ioe);
            }
            System.out.println("finally");
        }
        System.out.println("Have a nice day!");
        MyNiceExceptionDemo myNiceExceptionDemo = new MyNiceExceptionDemo();
        try {
            System.out.println("File size is: " + myNiceExceptionDemo.getSizeInMegabytes());
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
    }

    public int getSizeInMegabytes() throws IOException{
        return getSizeInKilobytes()/1024;
    }

    public int getSizeInKilobytes() throws IOException {
        int size = 0;

            size = getVal();
        return size/1024;
    }

    public int getVal() throws IOException {
        byte[] fileContents = Files.readAllBytes(Paths.get("/home/andriy/Douments/brainacad-java/p1/_rebrending-mainacad/Java_Part1_Module_2.9.pdf"));
        return fileContents.length;
    }
}
