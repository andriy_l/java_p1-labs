package com.mainacademy.primitive;

import java.util.Map;
import java.util.TreeMap;

/**
 * Created by andriy on 12.04.17.
 */
public class A {
    public static void main(String[] args) {
        args = new String[]{"A", "B","C","D","E","B","B","C","A"};

        Map<String, Integer > counter = new TreeMap<>();
        for (int i = 0; i < args.length; i++) {
            if(counter.containsKey(args[i])){
                Integer integ = counter.get(args[i]);
                counter.put(args[i],++integ);
            } else {
                counter.put(args[i],1);
            }
            System.out.println(counter);

        }
    }
}
