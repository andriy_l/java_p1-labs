package us.gov.whitehouse;

import java.math.BigDecimal;

/**
 * Created by andriy on 08.05.17.
 */
public class Animal extends God{
    public String name;

    public void sleep(){
        System.out.println("I like to sleep");
    }

    public Fish create(String name){
        return new Fish();
    }


    public BigDecimal add(Number n1, Number n2){
        return new BigDecimal(n1.doubleValue()+n2.doubleValue());
    }

    protected int eat(Food f ){
        if(f instanceof Worm){
        return 700;
        }
        else{
            return 100;
        }
    }

    protected int eat(){
        System.out.println("I like to eat");
        return 1;
    }

    private int code(){
        int linesOfCode = 10;
        return linesOfCode;
    }

    void jump(){
        System.out.println("I am cool animal");
    }

}
