package Laboratory1_6;


public class Lab2 {
    public static void main(String[] args) {
        int[] m = new int[]{10, 21, 5, 22, 9, 29, 25, 22, 11, 14, 8, 14};
        int sum = 0;
        int max = 0;
        int min = m[0];
        for (int i = 0; i < m.length; i++) {
            sum = sum + m[i];
            if (m[i] > max) {
                max = m[i];
            }
            if (m[i] < min) {
                min = m[i];
            }


        }
        int avg = sum / m.length;
        System.out.println("Max Value " + max);
        System.out.println("Min Value " + min);
        System.out.println("Average " + avg);
    }
}
