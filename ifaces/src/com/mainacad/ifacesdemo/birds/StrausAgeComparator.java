package com.mainacad.ifacesdemo.birds;

import java.util.Comparator;

public class StrausAgeComparator implements Comparator {

    @Override
    public int compare(Object o1, Object o2) {
        Straus s1 =  (Straus) o1;
        Straus s2 = (Straus) o2;
        int straus1Age = s1.getAge();
        int straus2Age = s2.getAge();
        if(straus1Age < straus2Age) return -1;
        if(straus1Age > straus2Age) return 1;
        return 0;
    }
}
