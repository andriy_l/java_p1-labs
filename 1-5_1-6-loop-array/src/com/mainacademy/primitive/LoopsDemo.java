package com.mainacademy.primitive;

import java.util.Scanner;

/**
 * Created by andriy on 18.04.17.
 */
public class LoopsDemo {
    public static void main(String[] args) {
        int n = Integer.parseInt(new Scanner(System.in).nextLine());
        for (int i = 1; i <= n; i++) {
            int sum = 0;

            for (int j = 1; j < i; j++) {
                if( i%j == 0) {
//                    System.out.println(i+ " Divisor: "+j);
                    sum +=j;
                }
            }
            if(sum == i){
                System.out.println(i+ " Perfect number: " + i);
            }

        }
//        int j = 0;
//        label1:
//        for (int i = 1; i < 300; i++) {
//            if(j == 10){
//                break;// label1;
//            }
//                if(i%3 == 0 | i%4 == 0){
//                    j++;
//                    System.out.println(j+" "+i);
//                }
//
//
//        }
//        label4:
//        for (int i = 0; i < 10; i++) {
//            label2:
//            for (int k = 0; k < 10; k++) {
//                label3:
//                for (int l = 0; l < 10; l++) {
//                    if(l == 5) break label3;
//                    else if( k == 9) break label4;
//
//                }
//
//            }
//
//        }
    }
}
