package samples;

/**
 * Created by andriy on 4/7/17.
 */
public class FromTests {
    public static void main(String[] args) {
//        byte b1 = 10;
//        byte b2 = 20;
//        short b3 = b1 + b2;
//        System.out.println(b1);

        int marks = 8;
        int total = 10;
        System.out.println(total < marks && ++marks > 5); // false &&... права частина не обчислюється
        System.out.println(marks);
        System.out.println(total == 10 || ++marks > 10);  // true ||... права частина не обчислюється
        System.out.println(marks);

        float f = 1 / 2;
        double d = 555d;
        int i = 1 / 3;
//        float fff = 1.45;
        System.out.println(12345 + 5432l); // 17777

        int num1 = 12;                                                 // line 3

        float num2 = 17.8f;                                          // line 4

        boolean eJavaResult = true;                             // line 5

        boolean returnVal = num1 >= 12 && (num2 < 4.567 || eJavaResult == false);

        System.out.println(returnVal);                             // line 7
        int a = 100;
        int b = 20;
        System.out.println(! (a > 20));
System.out.println(a > 20 && b > 10);
 System.out.println(a > 20 || b > 10);
System.out.println(! (b > 10));

String s = "Hello";
System.out.println(true ? false : true == true ? false : true);
        System.out.println(Integer.toBinaryString(010)+" "+Integer.toBinaryString(4));
        System.out.println(1 | 4);
        System.out.println(a+""+~a);
         int aa = 10;

             aa = aa++ + aa + aa-- - aa-- + ++aa;
    }
}
