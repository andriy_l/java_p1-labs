package com.mainacademy.oop;

/**
 * Created by andriy on 26.04.17.
 */
public class Main2 {
    public static void main(String...args) {
        Animal hedgehog = new Animal();
        Animal dog = new Animal();
        String s1 = hedgehog.voice();
        System.out.println(s1);
//
//        String s2 = hedgehog.voice(3);
//       String s3 = hedgehog.voice((byte) 3);
//        String s4 = hedgehog.voice((short) 3);
        String s5 = hedgehog.voice("Rex","Golochka", "YYY");
        String s6 = hedgehog.voice("Rex");
        System.out.println(s5);
        System.out.println(s6);
        hedgehog.eating((byte)2);
        hedgehog.eating((char)2);
        hedgehog.eating((short)0);
        long size = 100000000000L;
        hedgehog.eating(size);
        hedgehog.eating(1000);

    }
}
