package samples;

/**
 * Created by andriy on 4/6/17.
 */
public class BitOperationsDemo2 {
    public static void main(String args[]) {
        int a = 60;	/* 60 = 0011 1100 */
        int b = 13;	/* 13 = 0000 1101 */
        int c = 0;

        byte b1 = 0b0111_1111;
        byte b2 = 0b0000_0000;

        c = b1 & b2;        /* 12 = 0000 1100 */
        System.out.println("b1 & b2 = " + c + " " + Integer.toBinaryString(c) );

        c = a | b;        /* 61 = 0011 1101 */
        System.out.println("a | b = " + c );

        c = a ^ b;        /* 49 = 0011 0001 */
        System.out.println("a ^ b = " + c );

        c = ~a;           /*-61 = 1100 0011 */
        System.out.println("~a = " + c );

        c = a << 2;       /* 240 = 1111 0000 */
        System.out.println("a << 2 = " + c );

        c = a >> 2;       /* 15 = 1111 */
        System.out.println("a >> 2  = " + c );

        c = a >>> 2;      /* 15 = 0000 1111 */
        System.out.println("a >>> 2 = " + c );
    }
}

