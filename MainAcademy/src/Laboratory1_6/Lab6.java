package Laboratory1_6;


import java.util.Arrays;

public class Lab6 {
    public static void main(String[] args) {
        double[] weather = {-18.9, -19.4, -7.2, -1.7, -1.7, 3.9, 6.1, 6.7, 1.1, -3.3, -10.6, -14.4, -19.4};
        for (int i = 1; i < weather.length; i++)
            if (weather[i] < 0 & weather[i - 1] > 0) {
                for (int j = i; j > 0; j--)
                    if (weather[j - 1] > 0) {
                        double t = weather[j];
                        weather[j] = weather[j - 1];
                        weather[j - 1] = t;

                    }


                }
                System.out.println(Arrays.toString(weather));
            }

        }