package Laboratory1_6;


public class Lab7 {
    public static void main(String[] args) {
        int[][] matrix = {{1, 1, 1, 3, 4},
                {2, 1, 3, 1, 2},
                {2, 2, 3, 4, 1},
                {3, 3, 3, 1, 4}};
        for (int i = 0; i < matrix.length; i++){
            System.out.print("Line " + i + " : [");
            if (matrix[i][0] == 1)
                System.out.print(0 + (matrix[i][1] == 1 ? "-" : ", "));
            for (int j = 1; j < matrix[i].length - 1; j++)
                if (matrix[i][j] == 1 & (matrix[i][j - 1] != 1 | matrix[i][j + 1] != 1))
                    if (matrix[i][j + 1] != 1) {
                        int counter = 0;
                        for (int k = j + 1; k < matrix[i].length; k++)
                            if (matrix[i][k] == 1)
                                counter++;
                                System.out.print(j + (counter == 0 ? "" : ", "));
                            } else
                        System.out.print(j + "-");
            if (matrix[i][matrix[i].length - 1] == 1)
                System.out.print(matrix[i].length - 1);
            System.out.println("]");
        }

    }
}

