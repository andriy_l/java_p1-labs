package samples.InitSequence1;

public class InitSequence {
    static {  System.out.println("Static block #1"); }
    {         System.out.println("Non-static block #2");  }

    static { System.out.println("Static block #3");     }
    static String s;

    InitSequence(String s) {
        this.s = s;
        System.out.println("Constructor #4");
    }

    {   System.out.println("Non-static block #5");    }
        public static void main(String[] args) {
            System.out.println("main #6");
            InitSequence b1 = new InitSequence("string string string");
        }
    static {  System.out.println("Static block #7");   }
    {         System.out.println("Non-static block #8");     }
}
