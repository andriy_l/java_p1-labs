package cars;

/**
 * Created by andriy on 10.05.17.
 */
public class NasCar extends SportCar {
    int maxSpeed = 300;

    @Override
    public String toString() {
        return "NasCar{" +
                "maxSpeed=" + maxSpeed +
                ", name='" + name + '\'' +
                ", id=" + id +
                ", maxSpeed=" + maxSpeed +
                '}';
    }
}
