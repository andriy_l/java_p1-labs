package samples.InitSequence2;

class A {
    static String s;
    A(String s) {
        this.s = s;
        System.out.println("Constructor");
    }
    {
        System.out.println("Non-static block");
    }
    static {
        System.out.println("Static block");
    }
}

public class InitSequence {

        public static void main(String[] args) {
            A b1 = new A("1");
        }

}
