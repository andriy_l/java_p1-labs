/**
 * Created by andriy on 05.06.17.
 */
public class Outer {
    interface BI{}
    interface DI extends BI{}
    interface DDI extends DI{}
    interface EI extends DDI{}
    static class C<T>{}
    static void foo(C<? super DI> arg){} // lower bound
    static void m(C <? extends DI> arg){} // upper bound

    public static void main(String[] args) {
        m(new C<DI>());
        m(new C<DDI>());
        m(new C<EI>());
        m(new C()); // raw type of m(C<? extends DI>)
        foo(new C<BI>());
        foo(new C<BI>());
//        foo(new C<EI>());
        foo(new C());
//        foo(new C<DDI>()); // compile error
    }

}
