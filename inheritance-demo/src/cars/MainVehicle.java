package cars;

/**
 * Created by andriy on 10.05.17.
 */
public class MainVehicle {
    public static void main(String[] args) {
        Vehicle speedy1 = new SportCar("Petro");
        SportCar speedy2 = new SportCar("Petro");
        NasCar speedy3 = new NasCar();
        String str1 = new String("one");
        String str3 = str1; // true
        String str2 = new String("one");

        System.out.println(speedy1.equals(speedy2));
        System.out.println(speedy2.equals(speedy1));


    }
}
