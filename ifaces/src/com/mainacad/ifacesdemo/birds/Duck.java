package com.mainacad.ifacesdemo.birds;

import com.mainacad.ifacesdemo.foods.*;

/**
 * Created by andriy on 17.05.17.
 */
public class Duck extends Bird implements Comparable{

    public Duck(String name, double weigth, int age) {
        super(name, weigth, age);
    }

    @Override
    public void fly() {
        System.out.println("I can fly, because "
                + this.getClass().getSimpleName());
    }

    public void swim() {
        System.out.println(Duck.class.getName()+"s also can swim");
    }

    @Override
    public void eat(Food food) {
        if(food instanceof FastFood){
            this.setWeigth(this.getWeigth() * 1.5);
        } else if(food instanceof VitaminizedFood){
            this.setWeigth(this.getWeigth() * 1.1);
        } else if(food instanceof Insect){
            this.setWeigth(this.getWeigth() * 1.2);
        } else {
            this.setWeigth(this.getWeigth() * 1.01);
        }
    }

    @Override
    public void eatFF(FastFood food) {

    }

    @Override
    public void eatVitamin(VitaminizedFood v) {

    }

    @Override
    public int compareTo(Object o) {
        Duck that = (Duck) o;
        return (this.getAge() == that.getAge()? 0 :
                (this.getAge()<that.getAge()?-1:1));
    }
}
