package samples;

/**
 * Created by andriy on 4/6/17.
 */
public class BrainPrecedence {
    public static void main(String[] args) {
        int a = 5;
        int z = 0;
        int f = 0;
        int b = (f = z=(a + a)) * (f=(--a * a++));
        System.out.println(z + " " + b + " " + a + " " +f);

        System.out.println("====================================");
        boolean sold_more_than_100_units = true;
        int bonus_dollars = (sold_more_than_100_units) ? 50 : 0;
        System.out.println(bonus_dollars);
        System.out.println(true && true);
        System.out.println(true && false);
        System.out.println(false && true);
        System.out.println(false && false);
        System.out.println(true || true);
        System.out.println(true || false);
        System.out.println(false || true);
        System.out.println(false || false);
        int x = 0;
        boolean status = true && ++x == 0;
        System.out.println(x);
        status = false && ++x == 0;
        System.out.println(x);
        status = true || ++x == 0;
        System.out.println(x);
        status = false || ++x == 0;
        System.out.println(x);

    }
}
