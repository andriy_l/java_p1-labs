package ovrrdemo;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;

/**
 * Created by andriy on 15.05.17.
 */
public class Main {
    public static void main(String[] args) {
        Dad dad = new Child();
        Parent parent = new Child();


        Cloneable c;
        Serializable s;
        Child child = new Child();
        Parent parent1 = new Child();
        Mindset child2 = new Child();
        Wisdom child3 = new Child();
        child.getExperience();
        child.getExperience2();
        child.teach();

        System.out.println(Wisdom.getAdvice());
        System.out.println(child instanceof Wisdom);
        System.out.println(child instanceof Mindset);


        Parent p = null;
//        try {
            p = (Child) child.clone();
//        }catch (CloneNotSupportedException e){
//            System.out.println("Не можу клонувати");
//            e.printStackTrace();
//        }
        System.out.println(p);
        System.out.println(child);

//        // unchecked exception
//
        int[] a = new int[1];
        int i = 2;
//        try {
            a[--i] = 1 / --i; // ArithmeticException
//        }catch (ArithmeticException e){
//            System.out.println(e);
//        }
//
//        // checked exception
//        try {
//            FileWriter fileWriter = new FileWriter("andriy.txt");
//        }catch(IOException e){
//
//        }
    }
}
