package com.mainacad.guidemo;

import java.util.Arrays;
import java.util.Comparator;

public class PhoneBook {
    private PhoneNumber[] phoneNumbers = new PhoneNumber[10];
    private int counter = 0;
    private static int cnt;

    public void sortByName(){

        Comparator comparator = new Comparator(){
            @Override
            public int compare(Object o1, Object o2) {
                PhoneNumber p1 = (PhoneNumber)o1;
                PhoneNumber p2 = (PhoneNumber)o2;
                return p1.getName().compareToIgnoreCase(p2.getName());
            }
        };

        Arrays.sort(phoneNumbers, comparator);
    }

    public void sortByPhoneNumber(){
            Arrays.sort(phoneNumbers, new Comparator<PhoneNumber>() {
                @Override
                public int compare(PhoneNumber o1, PhoneNumber o2) {
                    return o1.getNumber().compareToIgnoreCase(o2.getNumber());
                }
            });
    }


    public static void sortByName(PhoneNumber[] phoneNumbers){
            Arrays.sort(phoneNumbers, new Comparator<PhoneNumber>() {
                        {

                        }
            public int compare(PhoneNumber p1, PhoneNumber p2){

                return p1.getName().compareToIgnoreCase(p2.getName());
            }
        }
            );
    }

    public static void sortByPhoneNumber(PhoneNumber[] phoneNumbers){

    }

    public void addPhoneNumber(String name, String phone){
            if(counter == phoneNumbers.length-1){
                System.out.println("Phone book is full");
            }else{
                phoneNumbers[counter++] = new PhoneNumber(name, phone);
            }
    }

    public void printPhoneBook(){
        for(int i = 0; i < counter; i++) {
            System.out.println(phoneNumbers[i]);
        }
    }



    static class PhoneNumber{
        private int pnCnt;
        private String name;
        private String number;

        @Override
        public String toString() {
            return "PhoneNumber{" +
                    "name='" + name + '\'' +
                    ", number='" + number + '\'' +
                    '}';
        }

        public PhoneNumber(String name, String number) {
            this.name = name;
            this.number = number;
        }

        public String getName() {
            return name;
        }


        public String getNumber() {
            return number;
        }

    }
}
