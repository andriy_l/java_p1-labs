package com.mainacad.genericsdemo;

import java.io.Serializable;
import java.util.ArrayList;

public class MyTestMethod {

    class A<T>{
        private T t;
//        T[] array = new T[10];
    }

    public static void main(String[] args) {
        MyTestMethod myTestMethod = new MyTestMethod();
        A<String> stringA = myTestMethod.new A<>();
        Object o = stringA;
        A<String> stringA1 = (A)o;


    }

    public static <T extends Number>int calcNum (T[] array, T maxElem){
        int result = 0;
//            if(!(maxElem instanceof Number)){return 0;}
//            if(!(array instanceof Number[])){return 0;}
            double num = maxElem.doubleValue();
        for (int i = 0; i < array.length; i++) {
            if(array[i].doubleValue() > num){
                result++;
            }
        }
        return result;
    }

    public static <T extends Number&Serializable&Cloneable> int calcElements(T[] array){
        return array.length;
    }
}
