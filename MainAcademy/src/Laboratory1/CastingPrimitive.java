package Laboratory1;


public class CastingPrimitive {
    public static void main(String[] args) {
        byte b = 5;
        int c = --b * b++;
        // I. left to right
        // 1) b is 4
        // 2) b++ on next step b is 5
        // 3) multiply old b by old b
        // 4) result of multiplication is 16, b is 5
        System.out.println( c + " " + b);
                            //6
        int a = (b + b) - (--b * b++);
        // II.
        // first I: в правих дужках (16), а b = 5
        //
    }
}
