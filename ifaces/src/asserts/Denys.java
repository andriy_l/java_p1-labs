package asserts;

import javax.swing.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.util.ArrayList;
import java.util.Scanner;

public class Denys {
    public static void main(String[] args){

        File f = new File("file.txt");
        // try with resources
        // autoclosable
//        try(Scanner scanner = new Scanner(f)) {
//            while (scanner.hasNext()) {
//                System.out.println(scanner.nextLine());
//            }
//        }catch (FileNotFoundException fnfe){
//            System.out.println(fnfe);
//        }

        ArrayList al;

        try(Scanner scanner = new Scanner(f);
            FileWriter fileWriter2 = new FileWriter("file2.txt",true)) {
            while (scanner.hasNext()) {
                String str = scanner.nextLine();
                fileWriter2.write(str + "\n");
                fileWriter2.flush();
            }
                }catch (IOException i){
                    System.out.println(i);
                }

  }
}
