package ovrrdemo;

import java.io.IOException;

  public abstract class Dad implements Wisdom {
    private String name;
    public void assemblerCoding() throws Exception {
        System.out.println("mov ax, bx");
        System.out.println("add cx, bx");
    }

    public abstract double swim();

    public void speak(String str){
        System.out.println("I'm dad and i say: " + str);
    }

    public Dad(){
      this("Панас");
        System.out.println("def constructor");
    }

    public Dad(String name){
        this.name = name;
        System.out.println("non-def constructor");
    }

    public static double incomeCount(){
        return 10_000_000.00_00;
    }
}
