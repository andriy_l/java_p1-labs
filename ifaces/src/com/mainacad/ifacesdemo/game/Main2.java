package com.mainacad.ifacesdemo.game;

import com.mainacad.ifacesdemo.birds.Duck;
import com.mainacad.ifacesdemo.birds.Straus;

import java.util.Arrays;

/**
 * Created by andriy on 17.05.17.
 */
public class Main2 {
    public static void main(String[] args) {
        Straus[] stado = new Straus[3];
        Straus s1 = new Straus("Vasylyna", 20, 10);
        Straus s2 = new Straus("Ivanka", 25, 2);
        Straus s3 = new Straus("Ulyana", 15, 13);

        String[] straussesNames = {s1.getName(), s2.getName(), s3.getName()};
        int[] straussesAges = {s1.getAge(), s2.getAge(), s3.getAge()};
        stado[0] = s1;
        stado[1] = s2;
        stado[2] = s3;
        Duck[] ducks = new Duck[3];
        ducks[0] = new Duck("One",4, 3);
        ducks[1] = new Duck("Two",3, 1);
        ducks[2] = new Duck("Three",5, 10);
        for (Straus s: stado) {
            System.out.println(s);
        }
        Arrays.sort(stado);
        Arrays.sort(straussesNames);
        Arrays.sort(straussesAges);


        Arrays.sort(ducks);
        System.out.println(Arrays.toString(ducks));

        System.out.println(Arrays.toString(stado));
        System.out.println(Arrays.toString(straussesNames));
        System.out.println(Arrays.toString(straussesAges));



    }
}
