package com.mainacad.ifacesdemo.game;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class StringPoligon {
    public static void main(String[] args) throws IOException{

        Scanner scanner = new Scanner(new File("file.txt"));
        int sum = 0;
        while(scanner.hasNext()) {
            String str = scanner.nextLine();
            {
                Scanner scanner1 = new Scanner(str);
                String s = scanner1.next();
                System.out.print(s + " ");
                int i = scanner1.nextInt();
                sum += i;
                System.out.print(i + i + "\n");
            }
        }
        System.out.println(sum);
    }
}
