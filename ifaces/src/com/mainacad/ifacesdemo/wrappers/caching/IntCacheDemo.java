package com.mainacad.ifacesdemo.wrappers.caching;

/**
 * Created by andriy on 27.05.17.
 */
public class IntCacheDemo {
    public static void main(String[] args) {
        Integer i1 = 1000;
        Integer i2 = 1000;
        Integer i3 = new Integer(1000);
        Integer i4 = new Integer(1000);
        System.out.println(i1 == i2);
        System.out.println(i3 == i4);
        Boolean b1 = false;
        Boolean b2 = false;
        System.out.println(b1 == b2);

    }
    public static Long compute(Byte b, Integer i){
        Long result = b + i.longValue();
        long re = b + i;
        return result;
    }
}
