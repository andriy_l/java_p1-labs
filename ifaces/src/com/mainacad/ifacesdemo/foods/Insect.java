package com.mainacad.ifacesdemo.foods;

/**
 * Created by andriy on 17.05.17.
 */
public class Insect implements Food{

    @Override
    public String toString() {
        return "Insect{" +
                "name='" + getName() + '\'' +
                ", length=" + getLength() +
                '}';
    }

    public Insect(){

    }

    public Insect(String name, int length) {
        this.setName(name);
        this.setLength(length);
    }

    private String name;
    private int length;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }
}
