package com.mainacademy.oop;

/**
 * Created by andriy on 26.04.17.
 */
public class Animal {

//    public String voice(){
//        System.out.println("#1");
//        return "I'm you animal friend";
//    }
//
//    public String voice(int times){
//        System.out.println("#2");
//        String str = "";
//        for (int i = 0; i < times; i++) {
//            str += "I'm you animal friend,";
//        }
//        return str;
//    }
//
//    public String voice(byte age){
//        System.out.println("#3");
//        return "I'm "+age+" old animal";
//    }
//
//    public void voice(short age){
//        System.out.println("#4");
//        System.out.println("I'm "+age+" old animal");
//    }

    public String voice(String name){
        System.out.println("#one string");
        return "I'm "+name;
    }
        // varargs
    public String voice(String...name){
        System.out.println("#varargs");
        String result = "";
        for (int i = 0; i < name.length; i++) {
            result += name[i];
        }
        return "We are "+result;
    }

//    public void eating(int...meat){
//        System.out.println("vararg");
//
//    }
    public void eating(int meat)
    {
        System.out.println("int");
    }
    public void eating(short meat){
        System.out.println("short");
    }
    public void eating(byte meat){
        System.out.println("byte");
    }
    public void eating(final long meat){

        System.out.println("long");
    }



}
